export default class TanderElement {
  getTemplate(tander) {
    const template =
      ` <li class="tanders__item tander" data-id="${tander.id}">
          ${this.getHeader(tander.title)}
          <div class="tander__info">
            ${this.getPrice(tander.price)}
            ${this.getSquareAndFloors(tander.square, tander.floors)}
            ${this.getRegionAndShow(tander.region, tander.show)}
          </div>
          <footer class="tander__footer">
            <button class="button button_primary">Подробнее</button>
            <button class="button button_light ml-3 js-remove-tander">Удалить</button>
          </footer>
        </li>`
      ;
    return template;
  }

  getHeader(title) {
    return `<header class="tander__header"><h2>${title}</h2></header>`;
  }

  getPrice(price) {
    return `<div class="tander__budget tander">
        <h4>Бюджет:</h4>
        <p class="tander__price">${price}</p>
        <section class="tander__costs">
          <span class="tander__commission">Комисиия сервиса: 124000</span>
          <br>
          <span class="tander__commission">Оплата исполнителю: 124000</span>
        </section>
      </div>`;
  }

  getSquareAndFloors(square, floors) {
    return `<div class="tander__square tander">
      <h4>Площадь и этаж:</h4>
      <span>${square}</span>
      <br>
      <span>${floors}</span>
    </div>`;
  }

  getRegionAndShow(region, show) {
    return `<div class="tander__region tander">
      <h4>Регион и выставка:</h4>
      <span>${region}</span>
      <br>
      <span>${show}</span>
    </div>`;
  }
}
