import TanderElement from './TanderElement';

export default class Tanders {
  constructor(containerClass) {
    this.resultHtml = '';
    this.tanders = [];
    this.container = document.querySelector(containerClass);
    this.loading = true;
  }

  getTanders() {
    return this.tanders;
  }

  fetchTanders() {
    return fetch('http://www.mocky.io/v2/5b6062ba2f00005e004618f6')
      .then(response => response.json())
      .then(data => {
        this.tanders = data.tanders;
        this.renderTandersList(this.tanders);
      })
      .catch(err => console.log(err));
  }

  renderTandersList(tanders) {
    if(!tanders.length) {
      this.container.innerHTML = '';
      return;
    }
    tanders.forEach(tander => {
      const element = new TanderElement().getTemplate(tander);
      this.resultHtml += element;
      this.container.innerHTML = this.resultHtml;
    });

    this.subscribe();
  }

  subscribe() {
    const deleteBtns = this.container.querySelectorAll('.js-remove-tander');

    deleteBtns.forEach(button => {
      button.addEventListener('click', () => {
        this.removeItem(button);
      });
    });
  }

  removeItem(button){
    const elementId = button.closest('.tander').dataset.id;
    this.tanders = this.tanders.filter(tander => tander.id !== elementId);
    this.clear();
    this.renderTandersList(this.tanders);
  }

  clear() {
    this.resultHtml = '';
  }
}
