import './polyfills';
import Tanders from './components/Tanders';

document.addEventListener('DOMContentLoaded', () => {
  const tanders = new Tanders('.tanders__list');
  tanders.fetchTanders();
});
